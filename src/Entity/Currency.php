<?php

namespace App\Entity;

use App\Repository\CurrencyRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CurrencyRepository::class)
 */
class Currency
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $currency;

    /**
     * @ORM\Column(type="float")
     */
    private $buy;

    /**
     * @ORM\Column(type="float")
     */
    private $sell;

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    private $begins_at;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $office_id;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCurrency(): ?string
    {
        return $this->currency;
    }

    public function setCurrency(string $currency): self
    {
        $this->currency = $currency;

        return $this;
    }

    public function getBuy(): ?float
    {
        return $this->buy;
    }

    public function setBuy(float $buy): self
    {
        $this->buy = $buy;

        return $this;
    }

    public function getSell(): ?float
    {
        return $this->sell;
    }

    public function setSell(float $sell): self
    {
        $this->sell = $sell;

        return $this;
    }

    public function getBeginsAt(): ?\DateTimeImmutable
    {
        return $this->begins_at;
    }

    public function setBeginsAt(\DateTimeImmutable $begins_at): self
    {
        $this->begins_at = $begins_at;

        return $this;
    }

    public function getOfficeId(): ?string
    {
        return $this->office_id;
    }

    public function setOfficeId(?string $office_id): self
    {
        $this->office_id = $office_id;

        return $this;
    }
}
