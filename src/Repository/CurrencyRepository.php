<?php

namespace App\Repository;

use App\Entity\Currency;
use App\Service\Helper;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Currency>
 *
 * @method Currency|null find($id, $lockMode = null, $lockVersion = null)
 * @method Currency|null findOneBy(array $criteria, array $orderBy = null)
 * @method Currency[]    findAll()
 * @method Currency[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CurrencyRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Currency::class);
    }

    public function add(Currency $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Currency $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function addNewCurrency($request, Helper $helper): array
    {
        $request = $helper->jsonToBody($request);
        $currency = new Currency();
        $currency->setCurrency($request->get('currency'))
            ->setBuy($request->get('buy'))
            ->setSell($request->get('sell'));
        $inputDate = $request->get('begins_at');
        $beginsAt = (new \DateTimeImmutable($inputDate, null));
        $currency->setBeginsAt($beginsAt)
            ->setOfficeId($request->get('office_id'));
        $this->add($currency, true);

        return [
            "message" => "Валюта успешно добавлена"
        ];
    }

    public function getCurrency($request, Helper $helper, $date, $officeId)
    {
        $from = date("d.m.Y H:i:s", strtotime("-1 days", strtotime($date)));
        $to = date("d.m.Y H:i:s", strtotime("+1 days", strtotime($date)));
        $db = $this->_em->createQueryBuilder();
        $db->select('e')
            ->from('App:Currency', 'e')
            ->where('e.begins_at BETWEEN :from AND :to')
            ->andWhere('e.office_id = :officeId')
            ->setParameter('from', $from)
            ->setParameter('to', $to)
            ->setParameter('officeId', $officeId)
            ->getQuery();
        return $db->getQuery()->getResult();
    }
}
