<?php

namespace App\Controller;

use App\Repository\CurrencyRepository;
use App\Service\Helper;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MainController extends AbstractController
{
    /**
     * @Route("/addcurrency", name="add_currency", methods={"POST"})
     */
    public function newCurrency(Request $request, CurrencyRepository $currencyRepository, Helper $helper): JsonResponse
    {
        $data = $currencyRepository->addNewCurrency($request, $helper);
        $data = $helper->getResponseForm($data);
        return new JsonResponse($data, Response::HTTP_OK);
    }

    /**
     * @Route("/getcurrency", name="get_currency", methods={"POST"})
     */
    public function getCurrency(Request $request, CurrencyRepository $currencyRepository, Helper $helper): JsonResponse
    {
        $requestData = $helper->jsonToBody($request);
        $officeId = $requestData->get('office_id');
        $beginsAt = $requestData->get('at_date');
        $currencies = $currencyRepository->getCurrency($request, $helper, $beginsAt, $officeId);
        $data = [];
        foreach ($currencies as $currencyData) {
            $data[] = [
                "currency" => $currencyData->getCurrency(),
                "buy" => $currencyData->getBuy(),
                "sell" => $currencyData->getSell(),
                "begins_at" => date("d-m-Y H:i:s", $currencyData->getBeginsAt()->getTimestamp()),
                "office_id" => $currencyData->getOfficeId(),
            ];
        }
        if (!$data) {
            $data = [
                "message" => "Ничего не найдено",
            ];
        }

        $data = $helper->getResponseForm($data);

        return new JsonResponse($data, Response::HTTP_OK);
    }


}
