<?php

namespace App\Service;

use App\Services\CustomResponse;
use Symfony\Component\HttpFoundation\Request;

class Helper
{
    /**
     * @param Request $request
     * @return Request
     */
    public function jsonToBody(Request $request): Request
    {
        $data = json_decode($request->getContent(), true);
        if ($data === null) {
            return $request;
        }
        $request->request->replace($data);
        return $request;
    }

    /**
     * @param $code
     * @param $data
     * @param $requestId
     * @return array
     */
    public function getResponseForm($data): array
    {
        $array = [
            "status" => 'success',
            "data" => $data
        ];
        return $array;
    }

}